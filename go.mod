module go.mod

go 1.17

require (
	github.com/agext/levenshtein v1.2.3
	github.com/gorilla/mux v1.8.0
)
