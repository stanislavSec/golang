package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gorilla/mux"
)

const ADDRESS = ":8080"

var users Users

// Users struct which contains
// an array of users
type Users struct {
	Users []User `json:"users"`
}

// User struct which contains a name
// a type and a list of social links
type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"age"`
	Social Social `json:"social"`
}

// Social struct which contains a
// list of links
type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func readJsonFile(name string) {

	jsonFile, err := os.Open(name)

	if err != nil {
		log.Println(err)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &users)
	defer jsonFile.Close()
}

func getJson(w http.ResponseWriter, req *http.Request) {
	log.Println("Call function getJson")

	if len(users.Users) == 0 {
		readJsonFile("data.json")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	data, _ := json.Marshal(users)
	w.Write(data)
}

func getData(w http.ResponseWriter, req *http.Request) {

	log.Println("Call function getData")

	if len(users.Users) == 0 {
		readJsonFile("data.json")
	}

	for i := 0; i < len(users.Users); i++ {
		fmt.Fprintf(w, "\nUser Type: "+users.Users[i].Type)
		fmt.Fprintf(w, "\nUser Age: "+strconv.Itoa(users.Users[i].Age))
		fmt.Fprintf(w, "\nUser Name: "+users.Users[i].Name)
		fmt.Fprintf(w, "\nFacebook Url: "+users.Users[i].Social.Facebook)
		fmt.Fprintf(w, "\n\n")
	}
}

func main() {

	router := mux.NewRouter().StrictSlash(true)

	router.HandleFunc("/", getData)
	router.HandleFunc("/getData", getData)
	router.HandleFunc("/getJson", getJson)

	log.Println("Starting HTTP server at address", ADDRESS)
	err := http.ListenAndServe(ADDRESS, router)

	if err != nil {
		log.Fatal("Unable to initialize HTTP server", err)

		os.Exit(2)
	}
}
