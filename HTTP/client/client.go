package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

var users Users

// Users struct which contains
// an array of users
type Users struct {
	Users []User `json:"users"`
}

// User struct which contains a name
// a type and a list of social links
type User struct {
	Name string `json:"name"`
	// Type string `json:"type"`
	Age int `json:"age"`
	// Weight int    `json:"Weight"`
	Social Social `json:"social"`
}

// Social struct which contains a
// list of links
type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

const countOfRequests = 1
const url = "http://localhost:8080/"

func main() {
	for i := 0; i < countOfRequests; i++ {

		getData()
		// getJson()
	}
}

func getData() {

	resp := get("getData")
	defer resp.Body.Close()

	responseData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("\n")
	log.Println(string(responseData))
	fmt.Println("\n")
}

func getJson() {

	resp := get("getJson")
	defer resp.Body.Close()

	err := json.NewDecoder(resp.Body).Decode(&users)

	if err != nil {
		log.Println(err.Error(), http.StatusBadRequest)
	}

	fmt.Println("\n")
	log.Println(users)
	fmt.Println("\n")
}

func get(method string) http.Response {
	resp, err := http.Get(url + method)

	if err != nil {
		log.Fatalf("Failed GET Response: %s", err)
	}

	return *resp
}
