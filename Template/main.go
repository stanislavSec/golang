package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
)

// datový typ, jehož prvky budou vypisovány v šabloně
type Role struct {
	Name       string
	Surname    string
	Popularity int
}

var roles = []Role{
	Role{"Eliška", "Najbrtová", 4},
	Role{"Jenny", "Suk", 3},
	Role{"Anička", "Šafářová", 0},
	Role{"Sváťa", "Pulec", 3},
	Role{"Blažej", "Motyčka", 8},
	Role{"Eda", "Wasserfall", 0},
	Role{"Přemysl", "Hájek", 10},
}

func main() {
	// parseTemplate("template.txt")
	// parseHtmlTemplate("layout.html")
	parseHtmlTemplate("body.html")
}

func parseHtmlTemplate(templateFilename string) {

	buff, err := ioutil.ReadFile(templateFilename)
	buff2, err := ioutil.ReadFile("base.html")

	text := string(buff) + string(buff2)

	fmt.Print(text)

	tpl, err := template.New("tpl").Parse(text)

	err = tpl.Execute(os.Stdout, roles)
	if err != nil {
		panic(err)
	}
}

func parseTemplate(templateFilename string) {
	tmpl := template.Must(template.ParseFiles(templateFilename))

	err := tmpl.Execute(os.Stdout, roles)
	if err != nil {
		panic(err)
	}
}
