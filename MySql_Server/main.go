package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

/*
 * Tag... - a very simple struct
 */
type Tag struct {
	ID    int    `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}

func main() {
	fmt.Println("Go MySQL Tutorial")

	// Open up our database connection.
	// I've set up a database on my local machine using phpmyadmin.
	// The database is called testDb
	db, err := sql.Open("mysql", "myadmin:myadmin@tcp(0.0.0.0:3366)/database")

	// if there is an error opening the connection, handle it
	if err != nil {
		panic(err.Error())
	}

	// defer the close till after the main function has finished
	// executing
	defer db.Close()

	results, err := db.Query("INSERT INTO users (name, email) VALUES ('Luděk Sobota' , 'sobota@nedele.cz');")

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Execute the query
	results, err = db.Query("SELECT id, name, email FROM users LIMIT 100;")

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	for results.Next() {
		var tag Tag
		// for each row, scan the result into our tag composite object
		err = results.Scan(&tag.ID, &tag.Name, &tag.Email)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		// and then print out the tag's Name attribute
		fmt.Printf("\nID:\t%d \nName:\t%s\nEmail:\t%s \n\n\n", tag.ID, tag.Name, tag.Email)
	}

}
