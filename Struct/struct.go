package main

import (
	"fmt"
)

type person struct {
	name string
	age  int
}

var peoples []person

func newPerson(name string) *person {
	p := person{name: name}
	p.age = 42
	return &p
}

func (p person) show() {
	fmt.Print(" Name: " + p.name)
	fmt.Printf(" => age: %d", p.age)
	fmt.Print(" \n\n ")
}

func main() {

	peoples = append(peoples, person{"Bob", 20}, person{name: "Alice", age: 30})
	peoples = append(peoples, person{name: "Fred"}, person{name: "Ann", age: 40})

	s := person{name: "Sean", age: 50}

	peoples = append(peoples, s)

	fmt.Println(s.name)

	sp := &s
	fmt.Println(sp.age)

	sp.age = 51
	fmt.Println(sp.age)

	count := len(peoples)

	fmt.Println("\n\nTisk pole: \n")

	for i := 0; i < count; i++ {
		peoples[i].show()
	}

}
