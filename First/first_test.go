package main

import (
	"testing"
)

func getErrMsg(shutBe, result string) string {
	return "Should be: " + shutBe + " Instead: " + result
}

func TestHello(t *testing.T) {
	result := getHello("Standa")
	shutBe := "Hello Standa"

	if result != shutBe {
		t.Error(getErrMsg(shutBe, result))
	}
}

func TestHelloIsNotJarda(t *testing.T) {
	result := getHello("Jarda")
	shutBe := "Hello"

	if result == shutBe {
		t.Error(getErrMsg(shutBe, result))
	}
}
