package main

import (
	"container/list"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"os"
	"strconv"
	"time"

	"github.com/agext/levenshtein"
)

const (
	Pondeli = iota
	Utery
	Streda
	Ctvrtek
	Patek
	Sobota
	Nedele
)

type User struct {
	id     int64
	name   string
	weight int64
}

type UserInterface interface {
	getIdentyty() string
	changeWeight(newWeight int) int64
}

func (user User) getIdentyty() string {
	s := "Id: " + strconv.FormatInt(user.id, 10) + " name: " + user.name + " weigth: " + strconv.FormatInt(user.weight, 10)
	fmt.Println(s)
	return s
}

func (user *User) changeWeight(newWeight int64) int64 {

	user.weight = newWeight
	return user.weight
}

func main() {
	// hello("Standa")
	// numbers()
	// array()
	// workWithPeople()
	// days()
	// goRutine()
	// useLevenshtein()
	// printBase(20000)
	// useList()
	// useJson()
	useImage()

}

func useImage() {
	const width = 256
	const height = 256

	img := image.NewRGBA(image.Rect(0, 0, width, height))
	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			var red uint8 = uint8(x)
			var green uint8 = uint8((x + y) >> 1)
			var blue uint8 = uint8(y)
			c := color.RGBA{red, green, blue, 255}
			img.SetRGBA(x, y, c)
		}
	}

	outfile, err := os.Create("test.png")
	if err != nil {
		panic(err)
	}
	defer outfile.Close()
	png.Encode(outfile, img)
}

func useJson() {
	var a1 [10]byte
	var a2 [10]int32
	a3 := [10]int32{1, 10, 2, 9, 3, 8, 4, 7, 5, 6}
	a4 := []string{"www", "root", "cz"}
	a5 := []interface{}{1, "root", 3.1415, true, []int{1, 2, 3, 4}}
	matice := [4][3]float32{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
		{0, -1, 0},
	}

	a1_json, _ := json.Marshal(a1)
	fmt.Println(string(a1_json))

	a2_json, _ := json.Marshal(a2)
	fmt.Println(string(a2_json))

	a3_json, _ := json.Marshal(a3)
	fmt.Println(string(a3_json))

	a4_json, _ := json.Marshal(a4)
	fmt.Println(string(a4_json))

	a5_json, _ := json.Marshal(a5)
	fmt.Println(string(a5_json))

	matice_json, _ := json.Marshal(matice)
	fmt.Println(string(matice_json))
}

func printList(l *list.List) {
	for e := l.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}
}

func useList() {
	l := list.New()
	l.PushBack("foo")
	l.PushBack("bar")
	l.PushBack("baz")
	printList(l)
}

func printBase(number int64) {
	value := number
	for base := 2; base < 36; base++ {
		println(base, strconv.FormatInt(value, base))
	}
}

func useLevenshtein() {
	s1 := "Hello"
	s2 := "hello"
	println(levenshtein.Distance(s1, s2, nil))

	s1 = "Marta"
	s2 = "Markéta"
	println(levenshtein.Distance(s1, s2, nil))

	s1 = " foo"
	s2 = "jiný naprosto odlišný text nesouvisející s foo"
	println(levenshtein.Distance(s1, s2, nil))
}

func workWithPeople() {
	peoples := []User{
		{1, "Stanislav Šec", 92},
		{2, "Josef Rajtmajer", 75},
		{3, "Petr Náhlík", 105},
	}

	// user := User{10, "Luboš Vepřek", 122}

	// peoples[3] = user

	fmt.Println(peoples)

	for i := 0; i < len(peoples); i++ {
		peoples[i].getIdentyty()
		peoples[i].changeWeight(150)
	}

	fmt.Println(peoples)
}

func hello(name string) {
	fmt.Println("Hello " + name)
}

func getHello(name string) string {
	return "Hello " + name
}

func goRutine() {
	go hello("Standa")
	go hello("Jarda")
	go hello("Petr")
	go hello("Tomáš")

	hello("Pes")
	time.Sleep(2 * time.Second)
}

func days() {
	fmt.Printf("%d\n", Pondeli)
	fmt.Printf("%d\n", Streda)
	fmt.Printf("%d\n", Patek)
}

func array() {

	var a1 [10]byte
	var a2 [10]int32

	a3 := [10]int32{1, 10, 2, 9, 3, 8, 4, 7, 5, 6}

	fmt.Printf("Delka pole 1: %d\n", len(a1))
	fmt.Printf("Delka pole 2: %d\n", len(a2))
	fmt.Printf("Delka pole 3: %d\n", len(a3))

	var a [10]int

	fmt.Printf("Pole pred upravou: %v\n", a)

	for i := 0; i < len(a1); i++ {
		a[i] = i * 2
	}

	fmt.Printf("Pole po uprave:    %v\n", a)

	var matice [10][10]float32

	fmt.Printf("Matice:    %v\n", matice)
}

func numbers() {
	var a float32 = -1.5
	var b float32 = 1.5
	var c float32 = 1e30
	var d float32 = 1e-30

	var h float64 = 1e-300
	var e float64 = -1.5
	var f float64 = 1.5
	var g float64 = 1e300

	fmt.Println(a)
	fmt.Printf("Zaokrouhleno:   %.2f\n", a)

	fmt.Println(b)
	fmt.Printf("Zaokrouhleno:   %.2f\n", b)

	fmt.Println(c)
	fmt.Printf("Zaokrouhleno:   %.2f\n", c)

	fmt.Println(d)
	fmt.Printf("Zaokrouhleno:   %.2f\n", d)
	fmt.Println(e)
	fmt.Printf("Zaokrouhleno:   %.2f\n", e)

	fmt.Println(f)
	fmt.Printf("Zaokrouhleno:   %.2f\n", f)

	fmt.Println(g)
	fmt.Printf("Zaokrouhleno:   %.2f\n", g)

	fmt.Printf("Zaokrouhleno:   %.2f\n", h)

	fmt.Println(h)
}
