package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
)

func CreateFile(nameOfFile string) {
	file, err := os.Create("test.txt") // Truncates if file already exists, be careful!
	if err != nil {
		log.Fatalf("failed creating file: %s", err)
	}
	defer file.Close() // Make sure to close the file when you're done

	len, err := file.WriteString("The Go Programming Language, also commonly referred to as Golang, is a general-purpose programming language, developed by a team at Google.")

	if err != nil {
		log.Fatalf("failed writing to file: %s", err)
	}
	fmt.Printf("\nLength: %d bytes", len)
	fmt.Printf("\nFile Name: %s", file.Name())
}

func ReadFile(nameOfFile string) {

	data, err := ioutil.ReadFile(nameOfFile)
	if err != nil {
		log.Panicf("failed reading data from file: %s", err)
	}
	fmt.Printf("\nLength: %d bytes", len(data))
	fmt.Printf("\nData: %s", data)
	fmt.Printf("\nError: %v", err)
}

func start() {
	nameOfFile := "test.txt"

	fmt.Printf("########Create a file and Write the content #########\n")
	CreateFile(nameOfFile)

	fmt.Printf("\n\n########Read file #########\n")
	ReadFile(nameOfFile)
	info(nameOfFile)
	copy(nameOfFile, "copied.txt")
}

func copy(nameOfFile string, nameOfNewFile string) {

	copyFile, err := os.Open(nameOfFile)

	if err != nil {
		log.Fatal(err)
	}

	newFile, err := os.Create(nameOfNewFile)

	if err != nil {
		log.Fatal(err)
	}

	// Copy the bytes to destination from source
	bytesWritten, err := io.Copy(newFile, copyFile)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Copied %d bytes.", bytesWritten)

	// Commit the file contents
	// Flushes memory to disk
	err = newFile.Sync()
	if err != nil {
		log.Fatal(err)
	}
}

func info(nameOfFile string) {
	// Stat returns file info. It will return
	// an error if there is no file.
	fileInfo, err := os.Stat(nameOfFile)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("File name:", fileInfo.Name())
	fmt.Println("Size in bytes:", fileInfo.Size())
	fmt.Println("Permissions:", fileInfo.Mode())
	fmt.Println("Last modified:", fileInfo.ModTime())
	fmt.Println("Is Directory: ", fileInfo.IsDir())
	fmt.Printf("System interface type: %T\n", fileInfo.Sys())
}
