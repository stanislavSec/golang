module banked

go 1.17

require (
	github.com/bold-commerce/go-shopify/v3 v3.11.0
	github.com/shopspring/decimal v1.3.1
	github.com/shurcooL/graphql v0.0.0-20200928012149-18c5c3165e3a
)

require (
	github.com/google/go-querystring v1.0.0 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
)
