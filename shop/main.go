package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	goshopify "github.com/bold-commerce/go-shopify/v3"
	"github.com/shopspring/decimal"
	"github.com/shurcooL/graphql"
)

const permanentToken = "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"
const selfExtUrl = " http://7de9-90-182-197-21.ngrok.io"

func main() {
	http.HandleFunc("/createPayment", CreatePayment)
	http.HandleFunc("/pendPayment", PendPayment)
	http.HandleFunc("/refundPayment", RefundPayment)
	http.HandleFunc("/banked-wh-sent", BankedWHSent)

	http.HandleFunc("/authenticate", ShopifyAuthenticate)
	http.HandleFunc("/callback", ShopifyCallback)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic(err)
	}
}

func BankedWHSent(w http.ResponseWriter, r *http.Request) {
	all, err := ioutil.ReadAll(r.Body)
	fmt.Printf("Webhook: %v, %v, %+v", string(all), err, r.Header)
}

func resolvePayment(shopifyGid string) {

	var m struct {
		PaymentSessionResolve struct {
			PaymentSession struct {
				Id     graphql.ID
				Status struct {
					Code string
				}
				NextAction struct {
					Action  graphql.String
					Context struct {
						PaymentSessionActionsRedirect struct {
							RedirectUrl string
						} `graphql:"... on PaymentSessionActionsRedirect"`
					}
				}
			}
			UserErrors struct {
				Message graphql.String
			}
		} `graphql:"paymentSessionResolve(id: $id)"`
	}

	variables := map[string]interface{}{
		"id": *graphql.NewID(shopifyGid),
	}

	httpClient := &http.Client{
		Transport: &AdminTransport{
			AccessToken: permanentToken,
		},
	}

	client := graphql.NewClient("https://banked-dbhq.myshopify.com/payments_apps/api/graphql.json", httpClient)

	err := client.Mutate(context.Background(), &m, variables)
	if err != nil {
		fmt.Errorf("error: %v", err)
	}
}

func PendPayment(w http.ResponseWriter, r *http.Request) {

	query := r.URL.Query()
	shopifyGid := query.Get("shgid")
	//shopifyCheckoutUrl := query.Get("shck")

	var m struct {
		PaymentSessionPending struct {
			PaymentSession struct {
				Id         graphql.ID
				NextAction struct {
					Action  graphql.String
					Context struct {
						PaymentSessionActionsRedirect struct {
							RedirectUrl string
						} `graphql:"... on PaymentSessionActionsRedirect"`
					}
				}
			}
			UserErrors struct {
				Message graphql.String
			}
		} `graphql:"paymentSessionPending(id: $id, pendingExpiresAt: $pendingExpiresAt, reason: $reason)"`
	}

	type DateTime string
	type PaymentSessionStatePendingReason string
	variables := map[string]interface{}{
		"id":               *graphql.NewID(shopifyGid),
		"reason":           PaymentSessionStatePendingReason("BUYER_ACTION_REQUIRED"),
		"pendingExpiresAt": DateTime(time.Now().Format("2006-01-02T15:04:05Z")),
	}

	httpClient := &http.Client{
		Transport: &AdminTransport{
			AccessToken: permanentToken,
		},
	}

	client := graphql.NewClient("https://banked-dbhq.myshopify.com/payments_apps/api/2022-04/graphql.json", httpClient)

	err := client.Mutate(context.Background(), &m, variables)
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, m.PaymentSessionPending.PaymentSession.NextAction.Context.PaymentSessionActionsRedirect.RedirectUrl, http.StatusTemporaryRedirect)

	resolvePayment(shopifyGid)
}

func parseHtmlTemplate(templateFilename string) *template.Template {

	body, err := ioutil.ReadFile(templateFilename)
	if err != nil {
		fmt.Errorf("unable to read file: %v", err)
		return nil
	}

	base, err := ioutil.ReadFile("tpl/base.html")
	if err != nil {
		fmt.Errorf("unable to read file: %v", err)
		return nil
	}

	text := string(body) + string(base)
	tpl, err := template.New("tpl").Parse(text)
	if err != nil {
		fmt.Errorf("faild to parse: %v", err)
		return nil
	}

	return tpl

}

func ShopifyCallback(w http.ResponseWriter, r *http.Request) {
	app := getShopifyApp()

	if ok, _ := app.VerifyAuthorizationURL(r.URL); !ok {
		http.Error(w, "Invalid Signature", http.StatusUnauthorized)
		return
	}

	query := r.URL.Query()
	shopName := query.Get("shop")
	code := query.Get("code")
	token, err := app.GetAccessToken(shopName, code)
	fmt.Printf("token: %s, error: %v", token, err)

	parsedTp := parseHtmlTemplate("tpl/form.html")
	err = parsedTp.Execute(w, struct{}{})

	if err != nil {
		fmt.Errorf("cannot execute file: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// url := fmt.Sprintf("https://%s/services/payments_partners/gateways/0a3e5965779a280d472fae7fe91fb5c0/settings", shopName)
	// http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func ShopifyAuthenticate(w http.ResponseWriter, r *http.Request) {
	app := getShopifyApp()
	shopName := r.URL.Query().Get("shop")
	state := "nonce"
	authUrl := app.AuthorizeUrl(shopName, state)
	http.Redirect(w, r, authUrl, http.StatusFound)
}

func getShopifyApp() goshopify.App {
	app := goshopify.App{
		ApiKey:      "0a3e5965779a280d472fae7fe91fb5c0",
		ApiSecret:   "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",
		RedirectUrl: selfExtUrl + "/callback",
		Scope:       "read_products,read_orders,write_payment_gateways,write_payment_sessions,read_payment_gateways,read_payment_sessions",
	}
	return app
}

func RefundPayment(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("%+v", r)
	all, err := ioutil.ReadAll(r.Body)
	fmt.Printf("%+v, %+v", string(all), err)
}

func CreatePayment(w http.ResponseWriter, r *http.Request) {
	var sPData ShopifyPayment
	err := json.NewDecoder(r.Body).Decode(&sPData)
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Printf("Shopify called CreatePayment: %+v \n\n", sPData)

	shopDomain := r.Header.Get("Shopify-Shop-Domain")
	amount, _ := decimal.NewFromString(sPData.Amount)
	amount = amount.Mul(decimal.NewFromInt(100))
	//TODO !!! These data are paired with the BankeD, need to load them from somewhere !!!
	const accountNumber = "12345678"
	const sortCode = "010203"
	const bankedKey = "pk_test_nLZ6xh7HO5ItvWKdJCVeJA"
	const bankedSecret = "sk_test_fb960b66a58bb593756ba3f0cee2ccb5"

	processPaymentUrl := getProcessPaymentURL(sPData.PaymentMethod.Data.CancelUrl+"/processing", sPData.Gid)
	bankedReq := BankedCreateSession{
		ErrorUrl:   "https://error.com",
		SuccessUrl: processPaymentUrl,
		LineItems: []LineItem{
			{
				Name:     "Payment on " + shopDomain,
				Amount:   amount.IntPart(),
				Quantity: 1,
				Currency: "GBP",
			},
		},
		Reference: "test payment",
		Payee: Payee{
			Name:          fmt.Sprintf("%s %s", sPData.Customer.BillingAddress.GivenName, sPData.Customer.BillingAddress.FamilyName),
			AccountNumber: accountNumber,
			SortCode:      sortCode,
		},
		Payer: Payer{
			Name:  fmt.Sprintf("%s %s", sPData.Customer.BillingAddress.GivenName, sPData.Customer.BillingAddress.FamilyName),
			Email: sPData.Customer.Email,
		},
	}

	bankedCreateData, err := json.Marshal(bankedReq)
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	req, err := http.NewRequest(http.MethodPost, "https://api.banked.com/v2/payment_sessions", bytes.NewBuffer(bankedCreateData))
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(bankedKey, bankedSecret)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	var bankedSessionJson struct {
		BankedCheckoutUrl string `json:"url"`
		ID                string `json:"id"`
	}
	err = json.NewDecoder(resp.Body).Decode(&bankedSessionJson)
	if err != nil {
		fmt.Errorf("error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Printf("Banked replied to create session: %+v \n\n", bankedSessionJson)

	var shopifyResp struct {
		RedirectUrl string `json:"redirect_url"`
	}

	shopifyResp.RedirectUrl = bankedSessionJson.BankedCheckoutUrl
	if err = json.NewEncoder(w).Encode(shopifyResp); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func getProcessPaymentURL(shopifyCheckoutURL string, shGid string) string {
	u, _ := url.Parse(selfExtUrl + "/pendPayment")
	q := u.Query()
	q.Add("dbid", "__PAYMENT_ID__")
	q.Add("shck", shopifyCheckoutURL)
	q.Add("shgid", shGid)
	u.RawQuery = q.Encode()
	return u.String()
}

type ShopifyPayment struct {
	Id             string `json:"id"`
	Gid            string `json:"gid"`
	Group          string `json:"group"`
	Amount         string `json:"amount"`
	Currency       string `json:"currency"`
	Test           bool   `json:"test"`
	MerchantLocale string `json:"merchant_locale"`
	PaymentMethod  struct {
		Type string `json:"type"`
		Data struct {
			CancelUrl string `json:"cancel_url"`
		} `json:"data"`
	} `json:"payment_method"`
	ProposedAt time.Time `json:"proposed_at"`
	Customer   struct {
		BillingAddress struct {
			GivenName   string `json:"given_name"`
			FamilyName  string `json:"family_name"`
			Line1       string `json:"line1"`
			Line2       string `json:"line2"`
			City        string `json:"city"`
			PostalCode  string `json:"postal_code"`
			Province    string `json:"province"`
			CountryCode string `json:"country_code"`
			Company     string `json:"company"`
		} `json:"billing_address"`
		ShippingAddress struct {
			GivenName   string `json:"given_name"`
			FamilyName  string `json:"family_name"`
			Line1       string `json:"line1"`
			Line2       string `json:"line2"`
			City        string `json:"city"`
			PostalCode  string `json:"postal_code"`
			Province    string `json:"province"`
			CountryCode string `json:"country_code"`
			Company     string `json:"company"`
		} `json:"shipping_address"`
		Email       string `json:"email"`
		PhoneNumber string `json:"phone_number"`
		Locale      string `json:"locale"`
	} `json:"customer"`
	Kind string `json:"kind"`
}

type LineItem struct {
	Name     string `json:"name"`
	Amount   int64  `json:"amount"`
	Quantity int    `json:"quantity"`
	Currency string `json:"currency"`
}

type Payee struct {
	Name          string `json:"name"`
	AccountNumber string `json:"account_number"`
	SortCode      string `json:"sort_code"`
}

type Payer struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type Session struct {
	ErrorUrl   string     `json:"error_url"`
	SuccessUrl string     `json:"success_url"`
	LineItems  []LineItem `json:"line_items"`
	Reference  string     `json:"reference"`
	Payee      Payee      `json:"payee"`
	Payer      Payer      `json:"payer"`
}

type BankedCreateSession Session

type AdminTransport struct {
	AccessToken string
}

// RoundTrip http Transport RoundTrip interface
func (t *AdminTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	req.Header.Set("X-Shopify-Access-Token", t.AccessToken)
	return http.DefaultTransport.RoundTrip(req)
}
